import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { RegisterService } from '../register.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {

    user: any = { nome: '', email: '', cpf: '', senha: '' };
    confirPassword: string = '';

    constructor(private registerService: RegisterService) {}

    ngOnInit() {}

    // CADASTRAR USUÁRIO
    saveUser() {

        // COMFIRMAÇÃO DE SENHA
        if(this.user.senha != this.confirPassword) {
            console.log('Senhas não conferem!');
            this.user.senha = '';
            this.confirPassword = '';
            return;
        }

        if(this.user.nome == '' || this.user.email == '' || this.user.cpf == '' || this.user.email == '' || this.user.senha == '') {
            return;
        }
        this.registerService.updateSaveUsuario(this.user).subscribe( response => {
            console.log(response);
            this.user = {};
            this.confirPassword = '';
            console.log('Usuário salvo com sucesso!');
        }, error => {
            console.log(error);
        }); 
    }
}
