import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../shared/services/api.service';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient,
  private api: ApiService) { }

  public updateSaveUsuario(user) {
    return this.http.post(this.api.getBaseUrl() + 'usuarios/updateSaveUsuario', user, this.api.getOptions());
  }

}
