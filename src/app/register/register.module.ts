import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { SignupRoutingModule } from './register-routing.module';
import { SignupComponent } from './register/register.component';
import { SharedModule } from '../shared/shared.module';
import { RegisterService } from './register.service';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SignupRoutingModule,
    SharedModule
  ],
  declarations: [
    SignupComponent
  ],
  providers: [
    RegisterService
  ]
})
export class SignupModule { }
