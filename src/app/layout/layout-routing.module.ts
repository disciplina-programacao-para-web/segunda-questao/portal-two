import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'usuarios', loadChildren: '../cadastros/usuario/usuarios.module#UsuariosModule' },
            { path: 'usuarios/', loadChildren: '../cadastros/usuario/usuarios.module#UsuariosModule' },
            { path: 'usuarios/:id', loadChildren: '../cadastros/usuario/usuarios.module#UsuariosModule'},
            { path: 'cidades', loadChildren: '../cadastros/cidade/cidade.module#CidadeModule' },
            { path: 'cidades/', loadChildren: '../cadastros/cidade/cidade.module#CidadeModule' },
            { path: 'cidades/:id', loadChildren: '../cadastros/cidade/cidade.module#CidadeModule'},
            { path: 'megasena', loadChildren: '../games/megasena/megasena.module#MegasenaModule' },
            { path: 'megasena', loadChildren: '../games/megasena/megasena.module#MegasenaModule' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
