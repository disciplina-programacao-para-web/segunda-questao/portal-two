import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  
  // URL DA API - CONECTANDO COM SERVIDOR (IP E PORTA DO SERVIDOR)
  // private url = 'http://192.168.15.90:8181/second-question/api/';

  // URL DA API - PRODUÇÃO  (LOCALHOST E PORTA DO SERVIDOR)
  // private url = 'http://localhost:8181/second-question/api/';

  // URL LOCAL (DESENVOLVIMENTO)
  private url = 'http://localhost:8080/second-question/api/';

  private options;

  constructor(private http: HttpClient) {
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', 'Basic Y2xpZW50ZWFwcDpsMWcxbnYxc3Q0');

    this.options = {
      headers: headers
    };
  }
 
  public getBaseUrl() {
     return this.url;
   }

  public getOptions() {
    return this.options;
  }
  
}
