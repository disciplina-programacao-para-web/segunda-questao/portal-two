import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../services/api.service';

@Injectable({
  providedIn: 'root'
})
export class ModalConfJogoService {

  cartela = { apostaNum: '12', numCartela: '19', num: [ 2, 19, 27, 37, 39, 59 ] };

  constructor(private http: HttpClient,
    private api: ApiService) { }

  // BUSCANDO CARTELA EM FASE DE TESTE  
  public getCartela() {
    return this.cartela;
  }

  // BUSCANDO TODAS AS CARTELAS
  public getAllCartelaByIdAposta() {
    return this.http.get(this.api.getBaseUrl() + 'cartelas/getAllCartelas', this.api.getOptions());
  }

}
