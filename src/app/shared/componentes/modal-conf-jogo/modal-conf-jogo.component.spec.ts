import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalConfJogoComponent } from './modal-conf-jogo.component';

describe('ModalConfJogoComponent', () => {
  let component: ModalConfJogoComponent;
  let fixture: ComponentFixture<ModalConfJogoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalConfJogoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConfJogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
