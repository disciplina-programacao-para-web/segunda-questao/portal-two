import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ModalConfJogoService } from './modal-conf-jogo.service';

@Component({
  selector: 'app-modal-conf-jogo',
  templateUrl: './modal-conf-jogo.component.html',
  styleUrls: ['./modal-conf-jogo.component.scss']
})
export class ModalConfJogoComponent implements OnInit {

  body: string = '';
  titulo: string = '';
  closeResult: string;

  constructor(private modalService: NgbModal,
    private modalConfJogoService: ModalConfJogoService) { }

  cartela: any = [];
  aposta: any = [];
  numCartela: any = [];

  @Input() idAposta: any;
  @Input() idCartela: any;

  ngOnInit() {
  }

  // ABRINDO O MODAL
  open(content) {
    this.openModal(content);
  }

    // FECHANDO O MODAL
    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    // ABRINDO O MODAL DE ENVIO DE EMAILS
    openModal(content) {
        // this.getCartela();
        this.cartela = [];
        this.numCartela = [];
        this.getAllCartelaByIdAposta(this.idAposta);
        this.modalService.open(content, { size: 'lg', centered: true }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            if(reason == 'Cross click') {
                this.body = '';
                this.titulo = '';
                return;
            }
            
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        })
    }   

    // BUSCANDO CARTELA EM FASE DE TESTE
    getCartela() {
        this.cartela = this.modalConfJogoService.getCartela();
        console.log(this.cartela);
    }

    // BUSCANDO CARTELAS PELO NÚMERO DA APOSTA
    getAllCartelaByIdAposta(id) {
        this.modalConfJogoService.getAllCartelaByIdAposta().subscribe( (response: any) => {
            console.log(response);
            response.forEach(element => {
                if(element.aposta.id && element.aposta.id == this.idAposta) {
                    this.aposta = element.aposta;
                    this.numCartela.push(element.numCartela);
                    console.log('--> APOSTA: ' + JSON.stringify(this.aposta));
                    console.log('--> NUMCARTELA: ' + this.numCartela);
                }
            });
        }, error => {
            console.log(error);
        });
    }

}
