import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router) {}

    canActivate() {
        if (sessionStorage.getItem('isLoggedin')) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    }

    // LOGIN
    login(user) {
        sessionStorage.setItem('isLoggedin', 'true');
        sessionStorage.setItem('user', JSON.stringify(user));
        this.router.navigate(['/dashboard']);
      }

    // BUSCANDO USUÁRIO LOGADO
    getUserLogado() {
        return JSON.parse(sessionStorage.getItem('user'));
    }

    // LOGOUT - SAIR
    logout() {
        sessionStorage.removeItem('isLoggedin');
        sessionStorage.removeItem('user');
        this.router.navigate(['/login']);
    }
}
