import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../shared/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient,
    private api: ApiService) { }

   // AUTENTICA USUÁRIO E LOGA NA APLICAÇÃO
   public autenticacao(usuario) {
    return this.http.post(this.api.getBaseUrl() + 'usuarios/autenticaUser', usuario, this.api.getOptions());
  } 

}
