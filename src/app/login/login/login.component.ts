import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../../router.animations';
import { LoginService } from '../login.service';
import { AuthGuard } from '../../shared';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    user: any = { email: '', senha: '' };

    constructor(public router: Router,
        private loginService: LoginService,
        private authGuard: AuthGuard) {}

    ngOnInit() {}

    // MÉTODO QUE AUTENTICA O USUÁRIO
    onLoggedin() {
        console.log(this.user);
        this.loginService.autenticacao(this.user).subscribe( response => {
            console.log(response);
            if(response != null) {
                this.savingUser(response);
            } else {
                console.log('Atenção! Verifique os dados de acesso e tente novamente!');
            }
        }, error => {
            console.log(error);
            console.log('Atenção! Verifique os dados de acesso e tente novamente!');
        });
    }

    // MÉTODO QUE LOGA NA APLICAÇÃO
    savingUser(usuario) {
        this.authGuard.login(usuario);
    }

}
