import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilCidadeComponent } from './perfil-cidade.component';

describe('PerfilCidadeComponent', () => {
  let component: PerfilCidadeComponent;
  let fixture: ComponentFixture<PerfilCidadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerfilCidadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfilCidadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
