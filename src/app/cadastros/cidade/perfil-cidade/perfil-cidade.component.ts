import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CidadeService } from '../cidade.service';

@Component({
  selector: 'app-perfil-cidade',
  templateUrl: './perfil-cidade.component.html',
  styleUrls: ['./perfil-cidade.component.scss']
})
export class PerfilCidadeComponent implements OnInit {

  id: number;
  dForm: FormGroup;
  labelSuccess: string = '';
  legBtnSave: string = '';
  estados: any = [];
  cidade: any = {};
  now = new Date();

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private cidadeService: CidadeService) { }

  ngOnInit() {
    this.criaForm();
    this.estados = this.cidadeService.getEstados();
    this.activatedRoute.params.subscribe( params => {
      if (!(params && params.id && params.id != 'register')) {
        this.legBtnSave = 'Cadastrar cidade';
        this.labelSuccess = 'Cadastrar';
        this.setDateNow();
        return [];
      } else {
          console.log(params.id);
          this.id = +params['id'];
          this.legBtnSave = 'Atualizar cidade';
          this.labelSuccess = 'Salvar';
          this.findCidadeById(this.id);
      }
    });
  }

  // CRIANDO FORMULÁRIO
  criaForm() {
    this.dForm = this.fb.group({
      id: [null],
      nome: [null, Validators.required],
      populacao: [null, Validators.required],
      pib: [null, Validators.required],
      uf: [null, Validators.required],
      dataCriacao: [null, Validators.required],
    });
  }

  // SETANDO A DATA ATUAL
  setDateNow() {
    this.dForm.patchValue({
      dataCriacao: this.now.toLocaleDateString()
    });
  }

  // UPDATE SAVE CIDADE
  ngOnSubmit() {
    this.cidade.id = this.dForm.value.id;
    this.cidade.nome = this.dForm.value.nome;
    this.cidade.populacao = this.dForm.value.populacao;
    this.cidade.pib = this.dForm.value.pib;
    this.cidade.uf = this.formatUF(this.dForm.value.uf);
    this.cidade.dataCriacao = this.returnData(this.dForm.value.dataCriacao);
    this.cidadeService.updateSaveCidade(this.cidade).subscribe( (response: any) => {
      console.log(response);
      this.findCidadeById(response.id);
    }, error => {
      console.log(error);
    });
  }

  // FORMATANDO O ESTADO
  formatUF(estado: any) {
    let uf = estado.uf;
    return uf;
  }

  // RETORNANDO O USUÁRIO PELO ID
  findCidadeById(id) {
    return this.cidadeService.findCidadeById(id).subscribe( response => {
      this.cidade = response; 
      this.dForm.patchValue({
        id: this.cidade.id,
        nome: this.cidade.nome,
        populacao: this.cidade.populacao,
        pib: this.cidade.pib,
        uf: this.cidade.uf,
        dataCriacao: this.cidade.dataCriacao,
      });
      console.log(this.dForm);
      console.log(this.dForm.value);
      console.log('DEBUG <--');
    }, error => {
      console.log('Erro!Não foi possível carregar o usuário!');
      console.log(error);
    });
  }

  returnData(data) {
    let dataCriacao;
    if(data) {
      console.log(data);
      
      let dia = data.substr(0, 2);
      let mes = data.substr(3, 2);
      let ano = data.substr(6, 4);
      dataCriacao = dia + '/' + mes + '/' + ano;
    }
    return dataCriacao;
  }

}
