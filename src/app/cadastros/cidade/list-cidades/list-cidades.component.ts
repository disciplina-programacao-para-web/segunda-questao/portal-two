import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CidadeService } from '../cidade.service';


@Component({
  selector: 'app-list-cidades',
  templateUrl: './list-cidades.component.html',
  styleUrls: ['./list-cidades.component.scss']
})
export class ListCidadesComponent implements OnInit {

  cidades: any;

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private cidadeService: CidadeService) { }

  ngOnInit() {
    this.getAllCidades();
  }

  // CADASTRAR USUÁRIO
  openRegister() {
    this.router.navigate(['cidades/register']);
  }

  // ATUALIZAR CIDADE
  onEdit(cidade) {
    this.router.navigate(['cidades/' + cidade.id]);
  }

  // REMOVER CIDADE
  removeCidade(cidade) {
    this.cidadeService.removeCidade(cidade).subscribe(respomse => {
      this.getAllCidades();
    }, error => {
      console.log(error);
    });
  }

  // BUSCANDO TODAS AS CIDADES
  getAllCidades() {
    this.cidadeService.getAllCidades().subscribe( response => {
      this.cidades = response; 
    }, error => {
      console.log(error);
    });
  }

}
