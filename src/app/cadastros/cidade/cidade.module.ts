import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CidadeRoutingModule } from './cidade-routing.module';
import { ListCidadesComponent } from './list-cidades/list-cidades.component';
import { PerfilCidadeComponent } from './perfil-cidade/perfil-cidade.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CidadeService } from './cidade.service';

@NgModule({
  imports: [
    CommonModule,
    CidadeRoutingModule,
    SharedModule
  ],
  declarations: [
    ListCidadesComponent,
    PerfilCidadeComponent
  ],
  exports: [
    ListCidadesComponent,
    PerfilCidadeComponent
  ],
  providers: [
    CidadeService
  ]
})
export class CidadeModule { }
