import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListCidadesComponent } from './list-cidades/list-cidades.component';
import { PerfilCidadeComponent } from './perfil-cidade/perfil-cidade.component';

const routes: Routes = [
  { path: '', component: ListCidadesComponent },
  { path: '', component: PerfilCidadeComponent },
  { path: ':id', component: PerfilCidadeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CidadeRoutingModule { }
