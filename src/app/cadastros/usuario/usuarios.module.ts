import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListUsuariosComponent } from './list-usuarios/list-usuarios.component';
import { PerfilUsuarioComponent } from './perfil-usuario/perfil-usuario.component';
import { SharedModule } from '../../shared/shared.module';
import { UsuariosService } from './usuarios.service';
import { UsuariosRoutingModule } from './usuarios-routing.module';

@NgModule({
  imports: [
    CommonModule,
    UsuariosRoutingModule,
    SharedModule
  ],
  declarations: [
    ListUsuariosComponent, 
    PerfilUsuarioComponent
  ],
  exports: [
    ListUsuariosComponent, 
    PerfilUsuarioComponent
  ],
  providers: [
    UsuariosService
  ]
})
export class UsuariosModule { }
