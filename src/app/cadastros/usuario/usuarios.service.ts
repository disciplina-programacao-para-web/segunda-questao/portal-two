import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../shared/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(private http: HttpClient,
    private api: ApiService) { }

    // BUSCANDO TODOS OS USUÁRIOS
    public getAllUsuarios() {
      return this.http.get(this.api.getBaseUrl() + 'usuarios/getAllUsuarios', this.api.getOptions());
    }
    
    // UPDATE STATUS USUÁRIO
    public updateStatus(usuario) {
      return this.http.get(this.api.getBaseUrl() + 'usuarios/updateStatus/'+ usuario.status + '/' + usuario.id, this.api.getOptions());
    }

    // REMOVER USUÁRIO
    public removeUsuario(usuario) {
      return this.http.delete(this.api.getBaseUrl() + 'usuarios/removeUsuario/' + usuario.id, this.api.getOptions());
    }

    // BUSCANDO USUÁRIO PELO ID
    public findUserById(id) {
      return this.http.get(this.api.getBaseUrl() + 'usuarios/findUserById/' + id, this.api.getOptions());
    }

    // SAVE USUÁRIO
    public updateSaveUsuario(usuario) {
      return this.http.post(this.api.getBaseUrl() + 'usuarios/updateUser', usuario, this.api.getOptions());
    }
}
