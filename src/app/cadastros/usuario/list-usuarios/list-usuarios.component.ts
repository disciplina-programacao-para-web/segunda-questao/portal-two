import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { UsuariosService } from '../usuarios.service';

@Component({
  selector: 'app-list-usuarios',
  templateUrl: './list-usuarios.component.html',
  styleUrls: ['./list-usuarios.component.scss']
})
export class ListUsuariosComponent implements OnInit {

  usuarios: any = [];

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private usuariosService: UsuariosService) { }

  ngOnInit() {
    this.getAllUsuarios();
  }

   // BUSCANDO TODOS OS USUÁRIOS
   getAllUsuarios() {
    this.usuariosService.getAllUsuarios().subscribe((response: any) => {
      this.usuarios = response;
    }, error => {
      console.log(error);
    });
  }

  // EDITAR USUÁRIO
  onEdit(u) {
    this.router.navigate(['usuarios/' + u.id]);
  }

  // CADASTRAR USUÁRIO
  openRegister() {
    this.router.navigate(['usuarios/register']);
  }

  // UPDATE STATUS USUÁRIO 
  updateStatus(usuario) {
    this.usuariosService.updateStatus(usuario).subscribe( response => {
      console.log('Atualizando o associado!');
      this.getAllUsuarios();
    }, error => {
      console.log(error);
      console.log('Erro ao atualizar o associado!');
    });
  }

  // REMOVER USUÁRIO
  removeUsuario(usuario) {
    this.usuariosService.removeUsuario(usuario).subscribe(respomse => {
      this.getAllUsuarios();
    }, error => {
      console.log(error);
    });
  }

}
