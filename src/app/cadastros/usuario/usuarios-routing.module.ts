import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListUsuariosComponent } from './list-usuarios/list-usuarios.component';
import { PerfilUsuarioComponent } from './perfil-usuario/perfil-usuario.component';

const routes: Routes = [
    { path: '', component: ListUsuariosComponent },
    { path: '', component: PerfilUsuarioComponent },
    { path: ':id', component: PerfilUsuarioComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsuariosRoutingModule {}

    