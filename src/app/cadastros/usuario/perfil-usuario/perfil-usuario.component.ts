import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { UsuariosService } from '../usuarios.service';

@Component({
  selector: 'app-perfil-usuario',
  templateUrl: './perfil-usuario.component.html',
  styleUrls: ['./perfil-usuario.component.scss']
})
export class PerfilUsuarioComponent implements OnInit {

  id: number;
  dForm: FormGroup;
  legBtnSave: string = '';
  labelSuccess: string;
  usuario: any = {};

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private usuariosService: UsuariosService ) { }

  ngOnInit() {
    this.criaForm();
     // RECUPERANDO O ID PASSADO COMO PARÂMETRO NA URL
     this.activatedRoute.params.subscribe( params => {
      if (!(params && params.id && params.id != 'register')) {
        this.legBtnSave = 'Cadastrar usuário';
        this.labelSuccess = 'Cadastrar';
        return [];
      } else {
          console.log(params.id);
          this.id = +params['id'];
          this.legBtnSave = 'Atualizar usuário';
          this.labelSuccess = 'Salvar';
          this.findUserById(this.id);
        }
     });
  }

  // CRIANDO FORMULÁRIO
  criaForm() {
    this.dForm = this.fb.group({
      id: [null],
      nome: [null, Validators.required],
      email: [null, Validators.required],
      cpf: [null, Validators.required],
      dataNascimento: [null, Validators.required],
      status: [null, Validators.required],
      senha: [null, Validators.required]
    });
  }

   // RETORNANDO O USUÁRIO PELO ID
   findUserById(id) {
    return this.usuariosService.findUserById(id).subscribe( response => {
      this.usuario = response; 
      this.dForm.patchValue({
        id: this.usuario.id,
        nome: this.usuario.nome,
        email: this.usuario.email,
        cpf: this.usuario.cpf,
        dataNascimento: this.usuario.dataNascimento,
        status: this.usuario.status,
        senha: this.usuario.senha
      });
    }, error => {
      console.log('Erro!Não foi possível carregar o usuário!');
      console.log(error);
    });
  }

  // UPDATE SAVE USUÁRIO
  ngOnSubmit() {
    this.usuario.id = this.dForm.value.id;
    this.usuario.nome = this.dForm.value.nome;
    this.usuario.email = this.dForm.value.email;
    this.usuario.cpf = this.dForm.value.cpf;
    this.usuario.dataNascimento = this.returnData(this.dForm.value.dataNascimento);
    this.usuario.status = this.dForm.value.status;
    this.usuario.senha = this.dForm.value.senha;
    this.usuariosService.updateSaveUsuario(this.usuario).subscribe( response => {
      console.log(response);
    }, error => {
      console.log(error);
    });
  }

  returnData(data) {
    let dataNascimento;
    if(data) {
      console.log(data);
      
      let dia = data.substr(0, 2);
      let mes = data.substr(2, 2);
      let ano = data.substr(4, 8);
      dataNascimento = dia + '/' + mes + '/' + ano;
    }
    return dataNascimento;
  }

}
