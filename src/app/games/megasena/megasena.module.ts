import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MegasenaRoutingModule } from './megasena-routing.module';
import { MegasenaPlayComponent } from './megasena-play/megasena-play.component';
import { MegasenaService } from './megasena.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { ListCartelasComponent } from './list-cartelas/list-cartelas.component';
import { ModalConfJogoComponent } from 'src/app/shared/componentes/modal-conf-jogo/modal-conf-jogo.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    MegasenaRoutingModule,
    SharedModule,
    NgbModule.forRoot(),
  ],
  declarations: [
    MegasenaPlayComponent,
    ListCartelasComponent,
    ModalConfJogoComponent
  ],
  exports:[
    MegasenaPlayComponent,
    ListCartelasComponent
  ],
  providers: [
    MegasenaService
  ]
})
export class MegasenaModule { }
