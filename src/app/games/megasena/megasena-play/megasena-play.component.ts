import { Component, OnInit } from '@angular/core';
import { MegasenaService } from '../megasena.service';
import { AuthGuard } from 'src/app/shared';
import { Router } from '@angular/router';

@Component({
  selector: 'app-megasena-play',
  templateUrl: './megasena-play.component.html',
  styleUrls: ['./megasena-play.component.scss']
})
export class MegasenaPlayComponent implements OnInit {

  user: any = {};
  numJogos: any = [];
  numDezenas: any = [];
  posicaoNum: number = 0;
  posicaoDez: number = 0;
  aposta: any = { quantJogos: '', quantDezenas: '', usuario: ''};
  param = 'cartelas';

  constructor(private router: Router,
    private authGuard: AuthGuard,
    private megasenaService: MegasenaService) {
      this.user = this.authGuard.getUserLogado(); 
    }

  ngOnInit() {
    this.numJogos = this.megasenaService.getArrayNum();
    this.numDezenas = this.megasenaService.getArrayDez();
    console.log(this.numJogos);
  }

  // SELECIONANDO QUANTIDADE DE JOGOS
  isSelectedNum(num) {
    this.posicaoNum = num;
    this.aposta.quantJogos = num;
  }

  // SELECIONANDO QUANTIDADE DE DEZENAS
  isSelectedDez(dez) {
    this.posicaoDez = dez;
    this.aposta.quantDezenas = dez;
  }

  // LIMPANDO DEFINIÇÕES DE APOSTAS
  clear() {
    this.posicaoNum = 0;
    this.posicaoDez = 0;
    this.aposta = {};
  }

  // ENVIADNO DADOS DA APOSTA AO SERVIDOR
  updateAposta() {
    this.aposta.usuario = this.user;
    if(this.aposta.quantDezenas && this.aposta.quantJogos ) {
      console.log('--> Tentando salvar aposta!');
      this.megasenaService.updateAposta(this.aposta).subscribe( response => {
        console.log(response);
        this.router.navigate(['megasena/' + this.param]);
      }, error => {
        console.log(error);
      });
    } else {
      console.log('--> Selecione os parâmetros da aposta!');
    }
  }

  // IR PARA LISTA DE APOSTAS
  goListGames() {
    this.router.navigate(['megasena/' + this.param]);
  }
}
