import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MegasenaPlayComponent } from './megasena-play.component';

describe('MegasenaPlayComponent', () => {
  let component: MegasenaPlayComponent;
  let fixture: ComponentFixture<MegasenaPlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MegasenaPlayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MegasenaPlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
