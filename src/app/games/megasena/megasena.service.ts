import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/shared/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class MegasenaService {

  // ARRAY COM OS NÚMEROS DE JOGOS À SEREM DEFINIDOS PELO USUÁRIO
  arrayNumeroJogos: any = [ 
    { pos : 1 }, { pos : 2 }, { pos : 3 }, { pos : 4 }, { pos : 5 },
    { pos : 6 }, { pos : 7 }, { pos : 8 }, { pos : 9 }, { pos : 10 }
  ];

  // ARRAY COM OS NÚMEROS DE DEZENAS À SEREM DEFINIDAS PELO USUÁRIO
  arrayDezenas: any = [ 
    { pos : 6 }, { pos : 7 }, { pos : 8 }, { pos : 9 }, { pos : 10 },
    { pos : 11 }, { pos : 12 }, { pos : 13 }, { pos : 14 }, { pos : 15 },
  ];

  constructor(private http: HttpClient,
    private api: ApiService) { }

  getArrayNum() {
    return this.arrayNumeroJogos;
  }

  getArrayDez() {
    return this.arrayDezenas;
  }

  // MANDANDO DADOS DA APOSTA
  public updateAposta(aposta) {
    return this.http.post(this.api.getBaseUrl() + 'megasena/updateAposta', aposta, this.api.getOptions());
  }

  // BUSCANDO TODAS AS APOSTAS DO USUÁRIO LOGADO
  public getAllApostasByUser(id) {
    return this.http.get(this.api.getBaseUrl() + 'apostas/getAllApostasByUser/' + id, this.api.getOptions());
  }

}
