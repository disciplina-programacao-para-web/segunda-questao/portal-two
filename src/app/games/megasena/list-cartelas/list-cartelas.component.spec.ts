import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCartelasComponent } from './list-cartelas.component';

describe('ListCartelasComponent', () => {
  let component: ListCartelasComponent;
  let fixture: ComponentFixture<ListCartelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCartelasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCartelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
