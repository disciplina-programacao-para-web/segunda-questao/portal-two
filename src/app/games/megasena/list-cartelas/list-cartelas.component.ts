import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MegasenaService } from '../megasena.service';
import { AuthGuard } from 'src/app/shared';

@Component({
  selector: 'app-list-cartelas',
  templateUrl: './list-cartelas.component.html',
  styleUrls: ['./list-cartelas.component.scss']
})
export class ListCartelasComponent implements OnInit {

  apostas: any;
  user: any = {};

  constructor(private router: Router,
    private megasenaService: MegasenaService,
    private authGuard: AuthGuard) { 
      this.user = this.authGuard.getUserLogado();  
    }

  ngOnInit() {
    this.getAllApostasByUser(this.user.id);
  }

  // VOLTAR À APOSTA
  goBack() {
    this.router.navigate(['megasena/']);
  }

  getAllApostasByUser(id) {
    this.megasenaService.getAllApostasByUser(id).subscribe( (response: any) => {
      console.log(response); 
      this.apostas = response;
    }, error => {
      console.log(error);
    });
  }

}
