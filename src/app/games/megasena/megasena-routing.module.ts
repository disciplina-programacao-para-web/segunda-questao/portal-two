import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MegasenaPlayComponent } from './megasena-play/megasena-play.component';
import { ListCartelasComponent } from './list-cartelas/list-cartelas.component';

const routes: Routes = [
  { path: '', component: MegasenaPlayComponent },
  { path: ':param', component: ListCartelasComponent }, 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MegasenaRoutingModule { }
